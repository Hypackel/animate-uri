<!DOCTYPE html>
<html lang="en">
<head>
  <base href="/contact/index.php">
  <!-- Translate Stuff -->
    <script type="text/javascript">
		function googleTranslateElementInit() {
			new google.translate.TranslateElement(
				{pageLanguage: 'en'},
				'google_translate_element'
			);
		}
        function MyFunction(){
            var x = document.getElementById("google_translate_element");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }}
	</script>
	
	<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
	</script>
  <!-- Title -->
    <title>Contact | Accumatic</title>

  <meta content="Automate all your factory reports and post directly to your DMS" name="description">
  <meta name="author" content="Aarav M">
  <meta content="Accumatic, Accumatic.com, Accumatic inc, DMS" name="keywords">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- HCapcha API -->
  <script src="https://js.hcaptcha.com/1/api.js" async defer></script>

  <!-- Favicons -->
  <link href="/assets/img/favicon.png" rel="icon">
  <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Main CSS File -->
  <link href="/assets/css/style.css" rel="stylesheet">

  <!-- JQuery API -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
     <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
  <a class="skip-nav-linker" href="/contact/index/#main">Skip Navigation</a>
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="/" class="logo d-flex align-items-center">
        <img src="/assets/img/logo.png" alt="">
      </a>


      <nav id="navbar" class="navbar">
        <ul>

          <li><a class="nav-link scrollto" href="/#about">Solution/Features</a></li>
          <li><a class="nav-link scrollto " href="/about/index">About Accumatic</a></li>
          <li><a class="nav-link scrollto active" href="/contact/index/">Contact Us</a></li>
          <li><a class="nav-link scrollto " target="_blank" rel="noopener noreferrer" href="https://dash.accumatic.com">Login</a></li>
          <!-- <li><a onclick="MiFunton();" class="instagram"><i style="font-size: 20px;" class="bi-xlg bi-translate"></i></a></li> -->
          <li><a class="getstarted scrollto" href="/contact/index/">Get Demo</a></li>
          <!-- <li><div style="display: none;" id="google_translateer"></div> </li> -->
          
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
  <br><br><br><br><br><br>
  <main id="main">
  <center><h1>The privacy policy is currently unavailable. Please try again later.</h1></center><br><br><br><br><br><br>
  </main>
  <script>
   
  </script>
  <?php require('footer.php');?> 

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="/assets/vendor/purecounter/purecounter.js"></script>
  <script src="/assets/vendor/aos/aos.js"></script>
  <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="/assets/vendor/php-email-form/validate.js"></script>

  <!-- Main JS File -->
  <script src="/assets/js/main.js"></script>
  <script>
    document.onreadystatechange = function() {
  if (document.readyState !== "complete") {
      document.querySelector("body").style.visibility = "hidden";
      document.querySelector("#preloader").style.visibility = "visible";
  } else {
      document.querySelector("#preloader").style.display = "none";
      document.querySelector("body").style.visibility = "visible";
  }
};
  </script>
</body>
</html>