  <!-- Translate Stuff -->
  <script type="text/javascript">function googleTranslateElementInit() {new google.translate.TranslateElement({pageLanguage: 'en'},'google_translate_element');}function MyFunctionwwe(){var x = document.getElementById("google_translate_element");if (x.style.display === "none") {x.style.display = "block"} else {x.style.display = "none";}}</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">

      <div class="footer-newsletter">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
              <h4>Our Newsletter</h4>
              <p>Subscribe to our newsletter to receive the latest news about us.</p>
            </div>
            <div class="col-lg-6">
              <form id="my-form" action="/footer-mailer.php" method="Post">
                <input type="email" name="email" required><button class="h-captcha" data-sitekey="52dfa3d7-88fb-4785-9e34-364798e0d46f"  data-callback="onSubmit" type="submit" style="position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    border: 0;
    background: none;
    font-size: 16px;
    padding: 0 30px;
    margin: 3px;
    background: #296aa6;
    color: #fff;
    transition: 0.3s;
    border-radius: 4px;">Subscribe</button>
    <script type="text/javascript">
  function onSubmit(token) {
    document.getElementById('my-form').submit();
    document.getElementById("my-form").action = "/submit.php";
  }
</script>
              </form>
            </div>
          </div>
        </div>
      </div>
  
      <div class="footer-top">
        <div class="container">
          <div class="row gy-4">
            <div class="col-lg-5 col-md-12 footer-info">
              <a href="index.html" class="logo d-flex align-items-center">
                <img src="/assets/img/logo.png" alt="">
              </a>
              <p>Cras fermentum odio eu feugiat lide<br> par naso tierra. Justo eget nada terra<br>
               videa magna derita valies darta donna<br>
                mare fermentum iaculis eu non diam phasellus.</p>
              <div class="social-links mt-3">
                <a href="tel:5715946336" class="twitter"><i class="fas fa-phone"></i></a>
                <a target="_blank" rel="noopener noreferrer" href="mailto:info@accumatic.com" class="facebook"><i class="far fa-envelope"></i></a>
                <a onclick="MyFunctionwwe();" class="instagram"><i class="bi bi-translate"></i></a>
                <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/accumatic" class="linkedin"><i class="bi bi-linkedin"></i></a>
              </div>
              <div style="display: none;" id="google_translate_element"></div> 
            </div>
  
            <div style="display: block;z-index: -99999999999999;" class="col-lg-2 col-6 footer-links">
              <h4>Useful Links</h4>
              <ul>
                <li><i class="bi bi-chevron-right"></i> <a href="/">Home</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="/#about">Solutions/Features</a></li>
                <li><i class="bi bi-chevron-right"></i> <a target="_blank" rel="noopener noreferrer" href="https://dash.accumatic.com">Login</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="#">Terms of service</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
              </ul>
            </div>
  
            <div  class="col-lg-2 col-6 footer-links">
            <h4>Useful Links</h4>
              <ul>
                <li><i class="bi bi-chevron-right"></i> <a href="/">Home</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="/#about">Solutions/Features</a></li>
                <li><i class="bi bi-chevron-right"></i> <a target="_blank" rel="noopener noreferrer" href="https://dash.accumatic.com">Login</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="/privacy">Terms of service</a></li>
                <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
              <h4>Contact Us</h4>
              <p>
                <a style="color: #444444;" href="https://goo.gl/maps/889vzUoTrSUgRPhz8" target="_blank" rel="noopener noreferrer">1100 Carillon Point,<br>
                Kirkland, WA 98033<br>
                United States <br><br></a>
                <strong>Phone:</strong> <a style="color: #444444;"  href="tel:5715946336">+1 (571) 594-6336</a><br>
                <strong>Email:</strong> <a style="color: #444444;" href="mailto:info@accumatic.com" target="_blank" rel="noopener noreferrer">info@accumatic.com</a><br>
              </p>
  
            </div>        
  
          </div>
        </div>
      </div>
  
      <div class="container">
        <div class="copyright">
          &copy; Copyright <?php echo date('Y');?> <strong><span>Accumatic</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          Designed by <a target="_blank" rel="noopener noreferrer" href="http://www.hypackel.com/contact-me">Aarav M</a>
        </div>
      </div>
    </footer><!-- End Footer -->