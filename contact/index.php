<!DOCTYPE html>
<html lang="en">
<head>
  <base href="/contact/index.php">
  <!-- Translate Stuff -->
    <script type="text/javascript">
		function googleTranslateElementInit() {
			new google.translate.TranslateElement(
				{pageLanguage: 'en'},
				'google_translate_element'
			);
		}
        function MyFunction(){
            var x = document.getElementById("google_translate_element");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }}
	</script>
	
	<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">
	</script>
  <!-- Title -->
    <title>Contact | Accumatic</title>

  <meta content="Automate all your factory reports and post directly to your DMS" name="description">
  <meta name="author" content="Aarav M">
  <meta content="Accumatic, Accumatic.com, Accumatic inc, DMS" name="keywords">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- HCapcha API -->
  <script src="https://js.hcaptcha.com/1/api.js" async defer></script>

  <!-- Favicons -->
  <link href="/assets/img/favicon.png" rel="icon">
  <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Main CSS File -->
  <link href="/assets/css/style.css" rel="stylesheet">

  <!-- JQuery API -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
     <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
  <a class="skip-nav-linker" href="/contact/index/#main">Skip Navigation</a>
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="/" class="logo d-flex align-items-center">
        <img src="/assets/img/logo.png" alt="">
      </a>


      <nav id="navbar" class="navbar">
        <ul>

          <li><a class="nav-link scrollto" href="/#about">Solution/Features</a></li>
          <li><a class="nav-link scrollto " href="/about/index">About Accumatic</a></li>
          <li><a class="nav-link scrollto active" href="/contact/index/">Contact Us</a></li>
          <li>
            <p style="color: white;z-index: -999999999999999;cursor: default; white-space: normal;visibility: hidden;">jujujujujujujujujujujujujujujujujujujuju</p>
          </li>
          <li><a class="nav-link scrollto " target="_blank" rel="noopener noreferrer" href="https://dash.accumatic.com">Login</a></li>
          <!-- <li><a onclick="MiFunton();" class="instagram"><i style="font-size: 20px;" class="bi-xlg bi-translate"></i></a></li> -->
          <li><a class="getstarted scrollto" href="/contact/index/">Get Demo</a></li>
          <!-- <li><div style="display: none;" id="google_translateer"></div> </li> -->
          
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
  <br><br>
  <main  id="main">
<!-- ======= Contact Section ======= -->
<section id="contact" class="contact">

  <div class="container" data-aos="fade-up">

    <header class="section-header">
      <h2>Contact</h2>
      <p>Contact Us</p>
    </header>

    <div class="row gy-4">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2687.389480333433!2d-122.20949558436769!3d47.65742967918806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5490132c711c6885%3A0xfc3ad4604af97cf0!2s1100%20Carillon%20Point%2C%20Kirkland%2C%20WA%2098033!5e0!3m2!1sen!2sus!4v1641768792420!5m2!1sen!2sus" width="600" height="320" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      <div class="col-lg-6">
        
        <div class="row gy-4">
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-geo-alt"></i>
              <h3>Address</h3>
              <a style="color: #444444;" href="https://goo.gl/maps/889vzUoTrSUgRPhz8" target="_blank" rel="noopener noreferrer"><p>1100 Carillon Point,<br>Kirkland, WA 98033</p></a>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-telephone"></i>
              <h3>Call Us</h3>
              <p><a style="color: #444444;"  href="tel:5715946336">+1 (571) 594-6336</a></p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-envelope"></i>
              <h3>Email Us</h3>
              <a style="color: #444444;" href="mailto:info@accumatic.com" target="_blank" rel="noopener noreferrer"><p>info@accumatic.com</a><br> <a style="color: #444444;" href="mailto:contact@accumatic.com" target="_blank" rel="noopener noreferrer">contact@accumatic.com</a></p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-box">
              <i class="bi bi-clock"></i>
              <h3>Open Hours</h3>
              <p>Monday - Friday<br>9:00AM - 06:00PM PST</p>
            </div>
          </div>
        </div>

      </div>

      <div class="col-lg-6">
        <form data-h-capcha-sitekey="75ae0b3a-c9ab-4c90-b840-615df6f39444" id="nicebtn" action="/submit.php" method="POST" class="php-email-form">
          <div class="row gy-4">

            <div class="col-md-6">
              <input autocomplete="example.com" type="text" name="name" class="form-control" placeholder="Your Name" required>
            </div>

            <div class="col-md-6 ">
              <input type="email" class="form-control" name="email" placeholder="Your Email" required>
            </div>

            <div class="col-md-12">
              <input autocomplete="example.com" type="text" class="form-control" name="subject" placeholder="Subject" required>
            </div>

            <div class="col-md-12">
              <textarea class="form-control" name="message" rows="6" placeholder="Message" required></textarea>
            </div>
            <br />
            <center><div class="h-captcha" data-theme="light" data-callback="hcaptchaCallback" data-sitekey="75ae0b3a-c9ab-4c90-b840-615df6f39444"></div></center>
            <script>
function hcaptchaCallback() {
    $('#sbatbtn').removeAttr('disabled');
    $('#searchsubmit').removeAttr("type").attr("type", "submit");
};
            </script>
            <br />
            <Center><div class="form-check form-group ps-0">
              <!-- <input id="privacy-policy" type="checkbox" name="privacy" value="accept" required> -->
              <label class="form-check-label ps-1" for="privacy-policy">
                By Sending a message you
                Accept our <a href="/terms.html">Terms of Service</a> and <a href="/privacy.html">Privacy Policy</a>
              </label>
            </div></Center>
            <div class="col-md-12 text-center">
              <div class="loading">Loading</div>
              <div class="error-message"></div>
              <div class="sent-message">Your message has been sent. Thank you!</div>

              <br />
              <button class="qwrabtn" id="sbatbtn" type="submit" disabled onmousedown="alert('Please complete the Captcha')">Send Message</button>
            </div>
            <center><p style="font-size: 15px;color: #808080;">This site is protected by hCaptcha and its
<a style="color: #808080;text-decoration: underline;" href="https://hcaptcha.com/privacy">Privacy Policy</a> and 
<a style="color: #808080;text-decoration: underline;" href="https://hcaptcha.com/terms">Terms of Service</a> apply.</center>
            </p>
          </div>
        </form>

      </div>

    </div>

  </div>

</section><!-- End Contact Section -->
  </main>

  <?php require('../footer.php');?> 

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="/assets/vendor/purecounter/purecounter.js"></script>
  <script src="/assets/vendor/aos/aos.js"></script>
  <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="/assets/vendor/php-email-form/validate.js"></script>

  <!-- Main JS File -->
  <script src="/assets/js/main.js"></script>
<style>
  .qwrabtn:hover{
background: #1e4e7a;
  }

  .qwrabtn{
    background: #296aa6;
    border: 0;
    padding: 10px 30px;
    color: #fff;
    transition: 0.4s;
    border-radius: 4px;
  }
        body, html{
            background: url('../assets/img/hero-bg.png') no-repeat;
        }
</style>
</body>
</html>