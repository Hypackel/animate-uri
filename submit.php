<!-- This is the Backend system to send an email
when a user submits the contact form 


Do NOT simply read the instructions in here without understanding
what they do.  They're here only as hints or reminders.  If you are unsure
consult Aarav M. You have been warned.  -->
<?php
// Loads PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

// Gets the form input
if ($_SERVER['REQUEST_METHOD'] === 'POST') {  
    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $email = $_POST['email'];
    $message = $_POST['message'];
}

$yourURL="http://accumatic.com";

// Defines the varible $mail is PHPMailer
$mail = new PHPMailer(true);

try {
	$mail->SMTPDebug = 0;									
	$mail->isSMTP();											
	$mail->Host	 = 'smtp.example.com;';	 // Put your SMTP Server here. ex. Gmail: smtp.gmail.com; Outlook: smtp-mail.outlook.com; Godaddy: smtpout.secureserver.net;			
	$mail->SMTPAuth = true;							
	$mail->Username = 'user@example.com';	// Your mail email to authenticate	
	$mail->Password = 'password';		//	Your mail password to authenticate	
	$mail->SMTPSecure = 'tls';			// The encryption that your mail server uses			
	$mail->Port	 = 587;                 // The port that your smtp server uses. ex. 465, 587

	$mail->setFrom($email);		
	$mail->addAddress('info@example.com'); // Put the email that you want to have the email sent to.
	
	$mail->isHTML(true);								
	$mail->Subject = $subject;
  $mail->AddEmbeddedImage('assets/img/logo.png', 'my-photo', 'favicon.jpg ');
  $mail->Body    = "<a href='https://accumatic.com'><img border='0' width='200' height='200' href='https://accumatic.com' class='sales' src='cid:my-photo' /></a><br><br><hr><br><h4>Hola,</h4>{$name} filled out your contact form on <a href='https://accumatic.com'>Accumatic.com</a><br><b>Name:</b><br> <center>{$name}</center><br><b>Email:</b> <center>{$email}</center> <br> <b>Subject:</b> <center>{$subject}</center> <br> <b>Message:</b> <center>{$message}</center> <br> That's all for now.";
	$mail->AltBody = "Name: {$name} Email: {$email} Subject: {$subject} Message: {$message} That's all for now.";
	$mail->send();
	echo ("<script>location.href='$yourURL'</script>");
} catch (Exception $e) {
    // I CANNOT STRESS THIS ENOUGH MAKE SURE TO FILL THE SMTP CREDENTIALS BECAUSE IF YOU DONT THEN IT WILL NOT WORK
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>
<!-- I CANNOT STRESS THIS ENOUGH MAKE SURE TO FILL THE SMTP CREDENTIALS BECAUSE IF YOU DONT THEN IT WILL NOT WORK -->

<?php
// $yourURL="http://www.hypackel.com#contact";
// echo ("<script>location.href='$yourURL'</script>");
?>