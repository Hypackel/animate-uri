# Accumatic
This repository is going to be Accumatic's new website<br><br>

# How to get to the new website
It will be eventuly on [Accumatic.com](https://accumatic.com). But it is currently on [accumatic.herokuapp.com](https://accumatic.herokuapp.com)<br><br>


# Dependencies

![Icnsb](https://img.shields.io/badge/Bootstrap%20Icons-1.7.2-blue)
![jquery](https://img.shields.io/badge/jquery-3.6.0-blue)
![Font Awesome](https://img.shields.io/badge/fontawesome-5.15.4-blue)
<br><br>

# License 
![unl](https://img.shields.io/badge/license-UNLICENSED-brightgreen)<br><br>

# Status
![Build](https://img.shields.io/badge/Build-In%20Progress-yellow)<br><br>
# Helpful Links
[![Foo](https://img.shields.io/badge/issues-report-red)](https://github.com/Hypackel/accumatic/issues/new)<br>

[![Foo](https://img.shields.io/badge/Website-accumatic.herokuapp.com-orange)](https://accumatic.herokuapp.com)<br>

[![Foo](https://img.shields.io/badge/Github-Hypackel%2Faccumatic-blue)](https://github.com/Hypackel/accumatic)

