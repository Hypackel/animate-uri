<!-- This is the Backend system to send an email
when a user submits the footer form 


Do NOT simply read the instructions in here without understanding
what they do.  They're here only as hints or reminders.  If you are unsure
consult Aarav M. You have been warned.  -->

<?php
// Loads PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

// Gets the form input
if ($_SERVER['REQUEST_METHOD'] === 'POST') {  
    $email = $_POST['email'];
}

// Defines the varible $mail is PHPMailer
$mail = new PHPMailer(true);

// SMTP Settings
try {
	$mail->SMTPDebug = 0;	// Leave this at 0 unless you are debugging set it to 2 or consult Aarav M		
	$mail->isSMTP();											
	$mail->Host	 = 'smtp.example.com;';	 // Put your SMTP Server here. ex. Gmail: smtp.gmail.com; Outlook: smtp-mail.outlook.com; Godaddy: smtpout.secureserver.net;			
	$mail->SMTPAuth = true;							
	$mail->Username = 'user@example.com';	// Your mail email to authenticate	
	$mail->Password = 'password';		//	Your mail password to authenticate	
	$mail->SMTPSecure = 'tls';			// The encryption that your mail server uses			
	$mail->Port	 = 587;                 // The port that your smtp server uses. ex. 465, 587

	$mail->setFrom($email);		
	$mail->addAddress('info@example.com'); // Put the email that you want to have the email sent to.
	
	$mail->isHTML(true);								
	$mail->Subject = 'Newsletter Subscription';
  $mail->AddEmbeddedImage('assets/img/logo.png', 'my-photo', 'favicon.jpg ');
  $mail->Body    = "<a href='https://accumatic.com'><img border='0' width='200' height='200' href='https://accumatic.com' class='sales' src='cid:my-photo' /></a><br><br><hr><br><h4>Hola,</h4>Sombody filled out your footer contact form on <a href='https://accumatic.com'>Accumatic.com</a><br><b>Their Email:</b>{$email}<br>That's all the info we have, Adios amigo.<br> <p style='color: red;text-transform: uppercase;'>This message will self destruct in 10 seconds</p>";
	$mail->AltBody = "Email: {$email} That's all for now.";
	$mail->send();
    echo "Mail has been sent successfully!";
} catch (Exception $e) {
    // I CANNOT STRESS THIS ENOUGH MAKE SURE TO FILL THE SMTP CREDENTIALS BECAUSE IF YOU DONT THEN IT WILL NOT WORK
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>
<!-- I CANNOT STRESS THIS ENOUGH MAKE SURE TO FILL THE SMTP CREDENTIALS BECAUSE IF YOU DONT THEN IT WILL NOT WORK -->